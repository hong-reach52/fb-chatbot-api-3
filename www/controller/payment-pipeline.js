const express = require('express');
const routerPayment = express.Router();
const model = require("../model/datahandler");
const requestPromise = require('request-promise');
const AllHtmlEntities = require('html-entities').AllHtmlEntities;
require('../config/config');

const entities = new AllHtmlEntities();

var receiptFormat = (parameters) => {
    return {  
        messages: [  
           {  
              attachment: {  
                 type: "template",
                 payload: {  
                    template_type: "receipt",
                    recipient_name: parameters.patientName,
                    order_number: parameters.payment_id,
                    currency: parameters.currency,
                    payment_method: parameters.payment_method,
                    order_url: "https://rockets.chatfuel.com/store?order_id=12345678901",
                    timestamp: Math.round(new Date().getTime()/1000),
                    address: parameters.address,
                    summary: parameters.summary,
                    adjustments :parameters.adjustments,
                    elements: parameters.elements
                 }
              }
           }
        ]
     }
};

routerPayment.post('/paymaya-payment', function(req, res) {
    //Base64 of:
    //pk-cP5SfWiULsViVtuswhEKCkuanfXdEkdF6mIRXDnH6A7:sk-X8qolYjy62kIzEbr0QRK1h4b4KDVHaNcwMYk39jInSl
    var options = {
        method: 'POST',
        uri: 'https://pg-sandbox.paymaya.com/payments/v1/payment-tokens',
        form: req.body[0],
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic cGstY1A1U2ZXaVVMc1ZpVnR1c3doRUtDa3VhbmZYZEVrZEY2bUlSWERuSDZBNzo='
        }
    };
     
    requestPromise(options)
        .then(function (body) {
        paymayaTokenResponse = JSON.parse(body)
        req.body[1].paymentTokenId = paymayaTokenResponse.paymentTokenId
        if(paymayaTokenResponse.state == "AVAILABLE") {
            var payOptions = {
                method: 'POST',
                uri: 'https://pg-sandbox.paymaya.com/payments/v1/payments',
                form: req.body[1],
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic c2stWDhxb2xZank2MmtJekVicjBRUksxaDRiNEtEVkhhTmN3TVlrMzlqSW5TbDo='
                },
            };
            requestPromise(payOptions)
            .then(function (body) {
                var paymayaStatusResponse = JSON.parse(body);
                res.send({"status" : paymayaStatusResponse.status})
            }).catch(function(err){
                res.send({"status" : "error"});
                console.log(err)
            });
        }
        else {
            res.send({
                "state": paymayaResponse.state,
                "paymentTokenId":paymayaResponse.paymentTokenId
            });
        }
    })
    .catch(function (err) {
        console.log(err.body)
    });

});

routerPayment.post('/ccard-token', function(req, res) {
    var options = {
        method: 'POST',
        uri: process.env.CHECKOUT_API_TOKEN_URL,
        body: req.body,
        headers: {
            "Authorization": process.env.CHECKOUT_PUBLIC_KEY,
            "Content-Type": 'application/json',
        },
        json: true
    };
    requestPromise(options)
        .then(function (body) {
            res.send({"token" : body.token})
    })
    .catch(function (err) {
        console.log(err.message)
        res.send({"status" : "error"})
    });

});

routerPayment.post('/ccard-payment', function(req, res) {
    var options = {
        method: 'POST',
        uri: process.env.CHECKOUT_API_URL,
        body: req.body,
        headers: {
            "Authorization": process.env.CHECKOUT_SECRET_KEY,
            "Content-Type": 'application/json',
        },
        json: true
    };
    console.log(req.body)
    requestPromise(options)
        .then(function (body) {
            res.send({"status" : body.status})
    })
    .catch(function (err) {
        console.log(err.message);
        res.send({"status" : "error"})
    });

});

routerPayment.post('/receipt', function(req, res) {
    var addressChunk = req.body.address.split(/,[ ]*/g);
    var receiptElements = JSON.parse(entities.decode(req.body.elements));

    var parameters = {
        "payment_id": req.body.payment_id,
        "payment_method": req.body.payment_method,
        "patientName": req.body.patientName,
        "currency":"PHP",
        "address": {
            "street_1": req.body.address.replace(addressChunk[addressChunk.length - 2] + ",", "").replace(addressChunk[addressChunk.length - 1], ""),
            "street_2": "",
            "city": addressChunk[addressChunk.length - 2],
            "postal_code": addressChunk[addressChunk.length - 1].replace("PH ", ""),
            "state": addressChunk[addressChunk.length - 1].replace(/[ ][0-9]+/g, ""),
            "country": addressChunk[addressChunk.length - 1].replace(/[ ][0-9]+/g, "")
         },
         "summary":{  
            "subtotal":0,
            "shipping_cost": req.body.shipping_cost,
            "total_tax": req.body.total_tax,
            "total_cost": parseFloat(req.body.total_cost.replace(/[a-zA-Z]+[ ]/g,""))
         },
         "adjustments":[{
              "name": "Discount",
              "amount": (-1)*req.body.discount
         }],
         "elements" :   receiptElements
    }
    
    res.json(receiptFormat(parameters));
});

module.exports = routerPayment;