const express = require('express');
const routerHealthCheckScoring = require('express').Router();
var moment = require('moment');

var errorCodes = require('../model/errorcodes.json');
var messageResponseCodes = require('../model/messageresponsecodes.json');
var responseButtons = require('../model/responsebuttons.json');

routerHealthCheckScoring.post('/get_score', function(req, res) {
  console.log(req.body)
  var lang = req.body.lang.substring(0,3);
  var attribute = req.body.attribute;
  var answer = req.body[attribute];
  var messageType = parseInt(String(req.body.messageType));  
  var block = req.body.block;
  var jsonResponse = {};
  var bmi = "";
  var userLang = lang=="eng" ? "" : "."+lang;
  
  if (messageType == 0){
    var totalScore = 14;
    var height = answer.height;
    var weight = answer.weight;
    if (!isNaN(height) && !isNaN(weight)) {
      var bmiComputation = computeBMI(height,weight);
      var bmi = getScore(bmiComputation.category, "bmi");
      if(bmi.score == null){
        totalScore =12;
      }
    } else { // if height and weight was not given an answer by user
      var bmi = responseButtons[lang]["bmi"].find(item => item.code == "unknown");
      var bmiComputation = {category: bmi.text, result: 0};
      totalScore =12;
      bmi.score = 0;
    }

    var famHis = getFamHisScore(answer.famHisCondition);
    var smokingHabit = getScore(answer.smokingHabit, "smoking");
    var alcoholHabit = getScore(answer.alcoholHabit, "alcohol");
    var foodNutrition = getScore(answer.foodNutrition, "nutrition");
    var exerciseLevel = getScore(answer.exerciseLevel, "exercise");
    var userCondition = getConHisScore(answer.condition)
    var mentalCondition = getMentalScore(answer.mentalCondition);

    if (answer.hasOwnProperty("frequencyLevel")) {
      var frequencyLevel = getScore(answer.frequencyLevel, "frequency");
    } else {
      var frequencyLevel = responseButtons[lang]["frequency"].find(item => item.code == "unknown");
    }
    console.log(frequencyLevel, lang, "frequency")

    var finalScore = (totalScore + bmi.score + 
                      famHis.score + smokingHabit.score + 
                      alcoholHabit.score + mentalCondition.score +
                      frequencyLevel.score + userCondition.score +
                      exerciseLevel.score + foodNutrition.score)*100/totalScore;

    var conditions = answer.famHisCondition.split(',');
    var conditionHealthCondition = answer.condition.split(',')

    var message = "";
    conditions.forEach((item,i) => {
      str = i+1 + ". " + conditions[i] + "\n" 
      message = message.concat(str);
    });

    var conditionHealthMessage = "";
    conditionHealthCondition.forEach((item,i) => {
      str = i+1 + ". " + conditionHealthCondition[i] + "\n"
      conditionHealthMessage = conditionHealthMessage.concat(str)
    })

    block = getHealthCheckInterpretation(finalScore,totalScore);
    
    jsonResponse = {
      "redirect_to_blocks" :["ohc.msg." + block + userLang],
      "set_attributes" : {
        "total.score" : totalScore.toString(),
        "user.score" : parseFloat(finalScore).toFixed(1).toString(),
        "user.bmi.result.block" : bmi.code,
        "user.bmi" : bmiComputation.category + " - " + bmiComputation.result + ".",
        "user.fam.his.result.block" : famHis.code,
        "conditions" : message,
        "user.smoking.habit.result.block" : smokingHabit.code,
        "user.alcohol.habit.result.block" : alcoholHabit.code,
        "user.food.nutrition.result.block" : foodNutrition.code,
        "user.exercise.level.result.block" : exerciseLevel.code,
        "user.frequency.meds": frequencyLevel.code,
        "user.condition.result.block": userCondition.code,
        "userConditions": conditionHealthMessage,
        "user.mental.condition.result.block" : mentalCondition.code
      }
    }

  }

  console.log("\n", jsonResponse,"\n")
  res.send(jsonResponse);


  function getHealthCheckInterpretation(score, totalScore){
    resultScore = ((score * totalScore)/100).toFixed(2);
    console.log("distinctScore : ", resultScore)
    if (resultScore <= totalScore-9 ) {
      return "very.high.risk";
    }
    else if (resultScore <= totalScore-5) {
      return "at.high.risk";
    }
    else if (resultScore <= totalScore-2) {
      return "at.risk";
    }
    else if (resultScore == totalScore-1) {
      return "at.low.risk";
    }
    else if (resultScore == totalScore) {
      return "healthy";
    }
    else {
      return "error";
    }
  }

  function computeBMI(height, weight){
    var category = "";
    var bmi = 0;
    try {
      weight = parseFloat(weight);
      height = parseFloat(height);
      if (isNaN(weight) && isNaN(height)) {
        throw(new(Error));
      }
      else {
        bmi = parseFloat(weight/((height/100) * (height/100))).toFixed(1);
        if(15.9 > bmi){
            category = "Severely underweight";
        }
        else if(16.0 <= bmi && 18.5 > bmi){
            category = "Underweight";
        }
        else if(18.5 <= bmi && 25.0 > bmi) {
            category = "Normal";
        } 
        else if(25.0 <= bmi && 30.0 > bmi) {
            category = "Overweight";
        } 
        else if(30.0 <= bmi){
            category = "Obese";
        }
      }
    }
    catch(e) {
      category = "";
    }  


    return {"category" : category, "result" : bmi};
  }

  function getScore(str, category){
    var result = {};
    responseButtons[lang][category].map(button =>{
      if (str.toLowerCase() == button.text.toLowerCase()) {
        result = button;
      }
    });
    return result;
  }

  function getFamHisScore(conditions){
    var score = 0;
    var code = "";
    if (conditions == "null") {
      score = 0;
      code = "none";
    }
    else if (conditions.split(",").length > 0) {
      score = -1;
      code = "not.none";
    }
    console.log("famhisScore : ", score);
    return {"score" : score, "code" : code};
  }

  
  function getConHisScore(conditions){
    var score = 0;
    var code = "";
    if (conditions == "null") {
      score = 0;
      code = "none";
    }
    else if (conditions.split(",").length > 0) {
      score = -1;
      code = "not.none";
    }
    console.log("conHisScore : ", score);
    return {"score" : score, "code" : code};
  }

  function getMentalScore (exp){
    var score = 0;
    var code = "";
    var ans  = exp.split(",");
    var isNone = ans.filter(item => {if(item =="6") return true;});
    console.log(ans.length);
    if (ans.length >= 1 && isNone.length == 0) {
      score = -2;
      code = "not.none";
    }
    else if (ans.length == 1 && isNone.length > 0) {
      score = 0;
      code = "none";
    }
    console.log("mentalScore : ", score);
    return {"score" : score, "code" : code};
  }

});


module.exports = routerHealthCheckScoring ;