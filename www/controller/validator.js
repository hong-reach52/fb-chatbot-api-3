const express = require('express');
const routerValidator = require('express').Router();
var moment = require('moment');
var dataset = require('../model/datasets/diseases.json');
var services = require('../model/datasets/services.json');
var errorCodes = require('../model/errorcodes.json');
var messageResponseCodes = require('../model/messageresponsecodes.json');
const { GridFSBucketReadStream } = require('mongodb');

routerValidator.post('/validator', function(req, res) {
    console.log(req.body)
    var lang = req.body.lang.substring(0,3);
    var messageType = parseInt(String(req.body.messageType));
    var attribute = req.body.attribute;
    var answer = req.body[attribute];
    var block = req.body.block;
    var skipBlock = req.body.skipBlock;
    var jsonResponse = {};
    var answers = "";
    var redirected = false;
    var maxRange = parseInt(String(req.body.maxRange))
    var errParse = false;
    var serviceRef = req.body.ref;
    var searchResults = req.body.searchResults;
    var key = req.body.key;
    var userLang = lang=="eng" ? "" : "." + lang;
    
  //for multiple choice input
  if(messageType == 0) {
    try {
      try{
        if (JSON.parse(answer) && answer.length > 1 ) {
          answers = JSON.parse(answer);

          if (answers.length == 1) {
            answers = answers[0].toString().split(",");            
          }
          else {
            if (answers > maxRange) {
              throw(new Error());
            }
          }

        }
        else{
          throw(new Error());
        }
      }
      catch(e){
        answers = answer.split(",");

      }

      if(Number(answers) === answers && answers % 1 !== 0){
        jsonResponse = setErrorResponse(block, errorCodes[lang]["err1"], serviceRef);   
      }
      else {
        var distinctAnswer = [];
        answers.filter(function(item, pos) {
            return  distinctAnswer.push(parseInt(item).toString());
        });

        var distinctAnswers = distinctAnswer.filter(function(item,pos){
          return distinctAnswer.indexOf(item) == pos
        });

        var isValidChoices =  distinctAnswers.every((item, index) => {
          if (errParse) {
            jsonResponse = setErrorResponse(block, errorCodes[lang]["err1"], serviceRef);
            return false;
          }
          else {
            item = parseInt(item);
            if (item == maxRange) {
              jsonResponse = cancelResponse(block);
              return false;
            }
            else if (item > maxRange || !between(item, 0, maxRange)) {
              
              jsonResponse = setErrorResponse(block,errorCodes[lang]["err2"], serviceRef);
              return false;
            }
            else if (item == 0) {
              jsonResponse = setNextBlock(block, ".done");
            }
            else{
              return true;
            }
          }
        });

          if (isValidChoices && serviceRef) {
            jsonResponse = setNextBlock("redirect.for."+block.split(".")[0]+ "s", "");
          }
          else if(isValidChoices && !serviceRef) {
            jsonResponse = setResponse(distinctAnswers, block);
          } 
        }
      }
    catch(e) {
      console.log(e);
    }
  }
  //for bday 
  else if(messageType == 1) {
    try {
      if (isNaN(answer)) {
        jsonResponse = setErrorResponse("age", errorCodes[lang]["err3"]);
      }
      else {
        age = parseInt(answer);
        if (age <= 0 || age > 200) {
          jsonResponse = setErrorResponse("age", errorCodes[lang]["err4"]);
        }
        else {
          jsonResponse = setAgeResponse(block,age);
        }
      }
    }
    catch(e) {
      console.log(e)
      jsonResponse = setErrorResponse("age", errorCodes[lang]["err3"]);
    }
  }
//for height
  else if(messageType == 2) {
    try {
      if (!isNaN(answer)) {
        var height = parseFloat(answer).toFixed(2);
        if (height > 230.0 ) {
          jsonResponse = setErrorResponse("height", errorCodes[lang]["err5"]);
        }
        else if (height < 50.0) {
          jsonResponse = setErrorResponse("height", errorCodes[lang]["err6"]);
        }
        else {
          jsonResponse = setHeightResponse(block,height);
        }
      }
      else {
        jsonResponse = setErrorResponse("height", errorCodes[lang]["err2"]);
      }
    }
    catch(e) {
      console.log(e);
      jsonResponse = setErrorResponse("height", errorCodes[lang]["err2"]);
    }

  }

//for weight
  else if(messageType == 3) {
    try {
      if (!isNaN(answer)) {
        var weight = parseFloat(answer).toFixed(2);
        if (weight > 300 ) {
          jsonResponse = setErrorResponse("weight", errorCodes[lang]["err7"])
        }
        else if (weight < 2) {
          jsonResponse = setErrorResponse("weight", errorCodes[lang]["err8"])
        }
        else {
          jsonResponse = setWeightResponse(block,weight);
        }
      }
      else {
        jsonResponse = setErrorResponse("weight", errorCodes[lang]["err2"]);
      }
    }
    catch(e) {
      console.log(e);
      jsonResponse = setErrorResponse("weight", errorCodes[lang]["err2"]);
    }

  }

  else if(messageType == 4) {
    try {
      if (!isNaN(answer)) {
        var age = parseFloat(answer).toFixed(2);
        if (age >= 13 ) {
          jsonResponse = setNextBlock(block, "");
        }
        else {
          jsonResponse = setNextBlock(skipBlock, "");
        }
      }
    }
    catch(e) {
      console.log(e);
    }
  }

  else if(messageType == 5) {
    try {
      if (!isNaN(answer.age)) {
        var age = parseFloat(answer.age).toFixed(2);
        if (age >= 13 && answer.gender == "female") {
          jsonResponse = setNextBlock(block, "");
        }
        else {
          jsonResponse = setNextBlock(skipBlock, "");
        }
      }
    }
    catch(e) {
      console.log(e);
    }
  }
//Select condition
  else if(messageType == 6) {
    try {

      var condition = answer.condition;
      var conditions = [];

      if (answer.selectedConditions!== "null") {
        conditions = answer.selectedConditions.split(',');
      }
      var foundstr = dataset[lang].diseases_and_other_conditions.find( record => {
        if (record.display == condition) {
          return record;
        }
      });

      if (foundstr) {
        if (conditions.includes(foundstr.display)) {
          conditions = conditions.toString();
          jsonResponse = setUserConditions(key,errorCodes[lang]["err9"], conditions);
        }
        else {
          conditions.push(foundstr.display);
          conditions = conditions.toString();
          var msg = messageResponseCodes[lang]["code1"].split("[]").join(foundstr.display)
          jsonResponse = setUserConditions(key,msg,conditions);
        }
      }

    }
    catch(e) {
      console.log(e);
    }
  } 

//search meds  
  else if(messageType == 7) {
    try {
      var medication = answer.medication;
      var foundstr = dataset.medications.filter( record => {
        const regex = new RegExp(medication,'gi');
        return record.display.match(regex)
      });

      if (foundstr.length>1) {
        jsonResponse = showMedicationItems(foundstr);
      }
      else if(foundstr.length == 1) {
        jsonResponse = {
          "redirect_to_blocks" : ["medication.select" + userLang],
          "set_attributes" : {"user.medication": foundstr[0].display.toString()}
        }
      }
      else if (foundstr.length<1) {
        jsonResponse = cantFindMedicationResponse(errorCodes[lang]["err10"])
      }
    }
    catch(e) {
      console.log(e);
    }
  } 

  //select medication

  else if(messageType == 8) {
    try {

      var medication = answer.medication;
      var medications = answer.selectedMedications;
      var userMeds = [];


      if (maxRange !=="null" && searchResults !== "null") {
        var searchItems = JSON.parse(searchResults);
        var answers = answer.medication.split(',');
        var distinctAnswer = answers.filter(function(item, pos) {
            errParse = isValidMultipleChoiceInput(item);
            return  answers.indexOf(parseInt(item).toString()) == parseInt(pos) ;
        });
        if (errParse) {
          jsonResponse = setSelectMedicationErrorResponse(block, errorCodes[lang]["err1"]);
        }
        else {
          var isValidChoices =  distinctAnswer.every((item, index) => {
            item = parseInt(item);
            if (item > maxRange || !between(item, 0, maxRange)) {  
              jsonResponse = setSelectMedicationErrorResponse(block,errorCodes[lang]["err2"]);
              return false;
            }
            else{
              return true;
            }
          });

          if (isValidChoices) {
            answers.map(selectItem=>{
              searchItems.items.map(searchItem => {
               if (searchItem.id == selectItem) {
                  userMeds.push(searchItem.display)
                }
              })
            });
            jsonResponse = setMedications(block,userMeds,medications, lang);
          }
        }
      }
      else {
        jsonResponse = setMedication(block,medication,medications, lang);
      }    
    }
    catch(e) {
      console.log(e);
    }
  }
  else if(messageType == 9){
    try{
      var service1 = getServiceItems(answer.service1, services.service1); 
      var service2 = getServiceItems(answer.service2, services.service2);
      var service3 = getServiceItems(answer.service3, services.service3);
      var service4 = getServiceItems(answer.service4, services.service4);

      var userServices = (service1!= "" ? service1 + "," : "") + (service2!= "" ? service2 + "," : "") +
            (service3!= "" ? service3 + "," : "") + (service4!= "" ? service4 : "");

      jsonResponse = setUserServices(userServices);
    }
    catch(e){
      console.log(e);
    }
  }
  else if (messageType == 10) {
    var conditions = answer.split(',');
    var message = "";
    conditions.forEach((item,i) => {
      str = i+1 + ". " + conditions[i] + "\n" 
      message = message.concat(str);
    });

    jsonResponse = {
      "redirect_to_blocks" : [block + userLang],
      "messages": [{"text":message}]
    }
  }
  console.log(jsonResponse);
  res.send(jsonResponse);

  function setUserServices(services){
    return {
      "set_attributes" : {
        "user.services" : services
      }
    }
  }

  function getServiceItems(items, services){
    var serviceItems = "";
    items = items.split(',');

    if (items.length == 1) {
      items = items.toString().split('.');
    }

    items.forEach((item, i) =>{
      
      if (!isNaN(item)) {
        var strItems = services.items.find((sItem, pos) => {
          if (item === sItem.id) {
            return true;
          }
          else {
            return false;
          }
        });
       
        if (items.length != i +1) {
          serviceItems += strItems !== undefined ? strItems.display + "," : "";

        }
        else if(items.length == i +1) {
          serviceItems += strItems !== undefined ? strItems.display : "";
        }
      }
    });
    return serviceItems;
  }

  function setMedications(block, medications, selectedMedications, lang){
    if (selectedMedications!== "null") {
      selectedMedications = selectedMedications.split(',');
    }
    else {
      selectedMedications = [];
    }

    var exists = false;
    selectedMedications.map(medication =>{
        medications.map(med =>{
            if(medication != med){
              selectedMedications.push(med);
            }
            else if(medication == med){
                exists = true;
            }
        });
    });

    var msg1 = messageResponseCodes[lang]["code3"].split("[]").join("("+medications.toString()+")");
    var msg2 = errorCodes[lang]["err11"];

    return setUserMedications(exists ? msg2 : msg1,selectedMedications);
  }


  function setMedication(block,medication,answers, lang){
    var medications = [];
    if (answers!== "null") {
      medications = answers.split(',');
    }

    var foundstr = dataset.medications.find( record => {
    if(record.display.match(new RegExp("\\b"+medication+".*","i"))){
        return record
      }
    });
    console.log(foundstr)
    if (foundstr) {
      if (medications.includes(foundstr.display)) {
        return setSelectMedicationErrorResponse(block,errorCodes[lang]["err11"]);
      }
      else {
        medications.push(foundstr.display);
        var msg = messageResponseCodes[lang]["code2"].split("[]").join(foundstr.display);
        return setUserMedications(msg,medications);
      }
    } 
    else {
      medications.push(medication);
      var msg = messageResponseCodes[lang]["code2"].split("[]").join(medication);
      return setUserMedications(msg,medications);
    }
  }

  function cantFindMedicationResponse(message){
    return {
      "messages" : [{"text":message}],
      "redirect_to_blocks" : ["medication.cant.find" + userLang]
    }
  }

  function showMedicationItems(items){
    var result = {"items" : []};
    var message = "";
    items.forEach((item,i) => {
      str = i+1 + ". " + item.display + "\n" 
      message = message.concat(str);
      result.items.push({"id" : i+1, "display" : item.display})
    });
    return {
      "set_attributes" : {
        "search.results" : JSON.stringify(result),
        "message" : message,
        "max_range" : result.items.length,
      },
      "redirect_to_blocks" : ["show.medications" + userLang]
    };
  }

  function setAgeResponse(block,age){
    return {
      "redirect_to_blocks" : [block + userLang],
      "set_attributes" : {
        "user.age" : age
      }
    }
  }

  function setHeightResponse(block,height){
    return {
      "redirect_to_blocks" : [block+ userLang],
      "set_attributes" : {
        "user.height" : height
      }
    }
  }

  function setWeightResponse(block,weight){
    return {
      "redirect_to_blocks" : [block + userLang],
      "set_attributes" : {
        "user.weight" : weight
      }
    }
  }

  function setResponse(items, block){
    var next_block = items[0];
    var response = "";
    items.shift();
    response = items.length == 0 ? setNextBlocks(block,next_block, "0") : 
    setNextBlocks(block, next_block, items);
    return response;
  }    
  function setNextBlocks(block, next_block, next_blocks){
    return {
      "redirect_to_blocks" : [block +"."+ next_block + userLang],
      "set_attributes" : {
        "next_blocks" : JSON.stringify(next_blocks)
      }
    }
  }

  function setNextBlock(block, str){
    return {
      "redirect_to_blocks" : [block + str + userLang]
    }
  }

  function setUserConditions(key, message, conditions){
    
    return {
      "redirect_to_blocks": ["conditions.selected.response" + userLang],
       "set_attributes" : {
        [key] : conditions,
        "message": message
      } 
    }
  }

  function setUserMedications(message, medications){
    var items = "";
    medications.forEach((item,i) =>{
      items = items.concat(i+1 + ". "+ item + "\n");
    });
    return {
      "redirect_to_blocks" : ["medication.add" + userLang],
      "messages": [{"text":message}],
      "set_attributes" : {
        "user.medications" : medications.toString(),
        "message" : items
      }
    }
  }

  function between(x, min, max) {
    return x >= min && x <= max;
  }

  function isValidMultipleChoiceInput(item) {
    return isNaN(item) || item.length == 0 || item == "" || Number.isInteger(item);
  }

  function setSelectMedicationErrorResponse(block, message) {

    return {
      "redirect_to_blocks": [block + userLang],
      "messages": [{
        "text":message
      }],
      "set_attributes" : {
        "message" : errorCodes[lang]["err12"]
      }
    }
  }

  function setErrorResponse(block, message, ref) {

    return !ref ? 
    {
      "redirect_to_blocks": [block + ".fallback.error" + userLang],
      "messages": [{
        "text":message
      }]
    }:
    {
      "redirect_to_blocks": [block +ref+ ".fallback.error" + userLang],
      "messages": [{
        "text":message
      }]
    }
  }
  function cancelResponse(block) {
    return {
      "redirect_to_blocks" : [block + ".fallback" + userLang]
    }
  }

});

module.exports = routerValidator ;