const axios = require('axios').default;
const { turnioToken } = require("../util/global/config")
const DiabetesSymptoms = require("../util/symptoms/diabetes")

axios.defaults.headers.common['Authorization'] = turnioToken;

exports.checkDiabetes = async (req, res) => {
    // turn io webhook body request
    let survery = req.body.messages
    let response = await DiabetesSymptoms(req.query.iso, survery)

    axios.post("https://whatsapp.turn.io/v1/messages/",
        {
            "text": {
                "body": `Thank you for answering our questions \n${response}`
            },
            "to": `${req.body.contacts[0].wa_id}`,
            "type": "text"
        }
    )

    res.send("done check diabetes")
}